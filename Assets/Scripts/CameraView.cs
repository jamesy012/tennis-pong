﻿using UnityEngine;
using System.Collections;

/// <summary>
/// lets the camera follow a object, but keeping it
/// </summary>
public class CameraView : MonoBehaviour {

	/// <summary>
	/// transform to follow
	/// </summary>
	public Transform m_Target;

	/// <summary>
	/// starting pos of camera
	/// </summary>
	private Vector3 m_StartPos;

	/// <summary>
	/// amount to damp the camera movement from it's starting pos
	/// </summary>
	public float m_Damping = 0.4f;

	/// <summary>
	/// gets starting pos
	/// </summary>
	void Start () {
		m_StartPos = transform.position;
	}
	
	/// <summary>
	/// moves camera to be (m_Damping * m_Target) pos away from it's starting pos
	/// </summary>
	void Update () {

		Vector3 movement = m_Target.position;
		movement.y = 0;
		transform.position = m_StartPos + (movement * m_Damping);
	}
}
