﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// gets if the player is wanting to hit the ball, and calulate how the player will hit the ball
/// </summary>
public class HitBall : MonoBehaviour {

	/// <summary>
	/// reference to map to get play area size
	/// </summary>
	public Map m_Map;
	/// <summary>
	/// transform of hit prediction 
	/// </summary>
	private Transform m_HitPos;

	/// <summary>
	/// key that this player uses to hit the ball
	/// </summary>
	private KeyCode m_HitKey;

	/// <summary>
	/// reference to Ball that has entered this players trigger
	/// set to null when ball leaves
	/// </summary>
	private GameObject m_ObjectToHit;

	//hit data
	/// <summary>
	/// reference to power slider
	/// </summary>
	public UnityEngine.UI.Slider m_PowerSlider;
	/// <summary>
	/// max distance from centerline when at max power
	/// </summary>
	public float m_MaxPower = 5;
	/// <summary>
	/// seconds it takes to get to max power
	/// </summary>
	public float m_TimeTillMaxPower = 2.0f;
	/// <summary>
	/// time the user started pressing the hit key
	/// </summary>
	private float m_StartHitTime;
	/// <summary>
	/// is the user trying hit something
	/// </summary>
	public bool m_IsHitting = false;
	/// <summary>
	/// amount of hit power, from 0 to m_MaxPower
	/// </summary>
	private float m_HitPower = 0;
	/// <summary>
	/// amount of horizontal distance from left and right movement of player
	/// </summary>
	public float m_HorionzintalMovementPower = 2.0f;

	/// <summary>
	/// storage of player num
	/// </summary>
	private int m_PlayerNum;

	/// <summary>
	/// gets components and hit key
	/// </summary>
	public void Start() {
		m_PlayerNum = transform.parent.GetComponent<MovePlayer>().m_PlayerNum;
		m_HitKey = DataManager.m_PlayerCodes[m_PlayerNum, 4];
		m_HitPos = GameObject.FindGameObjectWithTag("HitPos").transform;
	}

	/// <summary>
	/// updates power meter and checks if user has pressed or released hit key
	/// </summary>
	public void Update() {
		//code relating to slider/power gathering
		if (m_IsHitting) {
			//TODO MOVE THIS LINE INTO START WHEN NO MORE TWEEKING IS BEING DONE
			m_PowerSlider.maxValue = m_MaxPower;

			float deltaTime = Time.time-m_StartHitTime;
			float pow = deltaTime / m_TimeTillMaxPower;
			//pow = Mathf.Clamp01(pow);

			//this will cause it to go up then down then back up again
			pow %= 2;
			if(pow > 1) {
				pow = 1 + (1 - pow);
			}
			pow *= m_MaxPower;

			m_HitPower = pow;

			m_PowerSlider.value = m_HitPower;
		}

		//get if the hit key was pressed
		if (Input.GetKeyDown(m_HitKey)) {
			m_StartHitTime = Time.time;
			m_IsHitting = true;
			//print(m_StartHitTime);
		}

		//get if the hit key was released
		//and launch ball
		if (Input.GetKeyUp(m_HitKey)) {
			m_PowerSlider.value = 0;
			m_IsHitting = false;
			if (m_ObjectToHit == null) {
				return;
			}

			if((m_ObjectToHit.transform.position.z > 0 && transform.position.z < 0)|| (m_ObjectToHit.transform.position.z < 0 && transform.position.z > 0) ){
				print("Not on right side");
				return;
			}

			SetBallVel();
		}
	}

	/// <summary>
	/// ball has left
	/// </summary>
	/// <param name="other">unused</param>
	public void OnTriggerExit(Collider other) {
		m_ObjectToHit = null;
	}

	/// <summary>
	/// ball has entered
	/// sets m_ObjectToHit to collider game object
	/// </summary>
	/// <param name="other">reference to ball</param>
	public void OnTriggerEnter(Collider other) {
		Ball otherBall = other.GetComponent<Ball>();
		if (otherBall == null) {
			return;
		}
		if (!otherBall.m_CanHit) {
			return;
		}
		m_ObjectToHit = other.gameObject;
	}

	/// <summary>
	/// calculates the position of where the ball will land after getting hit
	/// takes into account, position and what direction the player it trying to move.
	/// sets ball prediction position to that location
	/// </summary>
	private void CalculateHitPos() {
		Vector3 pos = transform.position;
		pos.x = -pos.x;
		pos.z =  -0.5f + m_HitPower;

		//Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		Vector3 movement = MovePlayer.GetMovement(m_PlayerNum);
		if (movement.x != 0) {
			pos.x += movement.x * (m_HorionzintalMovementPower * UnityEngine.Random.Range(0.4f,1.0f));
			pos.x = Mathf.Clamp(pos.x, -(m_Map.m_Width+m_Map.m_Offset), m_Map.m_Width+m_Map.m_Offset);
		}
		//do same for vertical?

		//random


		if (transform.position.z > 0) {
			pos.z *= -1;
		}
		m_HitPos.position = pos;
	}

	/// <summary>
	/// sets the velocity to be where the hit has set the hitPrediction is
	/// also tells ball it was hit
	/// </summary>
	private void SetBallVel() {
		Rigidbody rb = m_ObjectToHit.GetComponent<Rigidbody>();
		if (rb == null) {
			print("NO RIGIDBODY ON OBJECT: " + m_ObjectToHit.name);
		}

		CalculateHitPos();

		Ball ball = m_ObjectToHit.GetComponent<Ball>();

		if(-Ball.GetSide(transform.position) == ball.m_CurrSide) {
			return;
		}

		ball.BallHit();

		Vector3 vel = BallLandPerdiction.CalcBallisticVelocityVector(m_ObjectToHit.transform.position, m_HitPos.position, 60);
		rb.velocity = vel;
	}


}
