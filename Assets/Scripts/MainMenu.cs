﻿using UnityEngine;
using System.Collections;

/// <summary>
/// scripts related to the main menu and changing scenes
/// </summary>
public class MainMenu : MonoBehaviour {

	/// <summary>
	/// opens the game scene,
	/// launched from unity events on main menu
	/// </summary>
	public void LaunchGame() {
		UnityEngine.SceneManagement.SceneManager.LoadScene(1);
	}

	/// <summary>
	/// quits the game,
	/// launched from unity events on main menu
	/// </summary>
	public void QuitGame() {
		Application.Quit();
	}

}
