﻿using UnityEngine;
using System.Collections;

/// <summary>
/// handles lose conditions of game, automated movement to center and the starting serve
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Ball : MonoBehaviour {

	/// <summary>
	/// counter of how many bounces there have been on this side
	/// </summary>
	public int m_BouncesOnCurrentSide = 0;
	/// <summary>
	/// what side was the ball last recorded being on
	/// </summary>
	public int m_CurrSide = 0;

	/// <summary>
	/// reference to Map to get the map sizes
	/// </summary>
	public Map m_Map;

	/// <summary>
	/// flag to check if the ball can be hit or not
	/// </summary>
	public bool m_CanHit = false;
	/// <summary>
	/// flag to check if the ball is resetting
	/// </summary>
	private bool m_BallResetting = false;
	/// <summary>
	/// after hitting a ball, it will get immunity from scoring to prevent bug of player hitting ball and it somehow causing them to lose a point
	/// </summary>
	private bool m_BallImmunity = false;

	/// <summary>
	/// time after the ball reaches the middle for when the ball will launch
	/// </summary>
	public float m_SecondsTillStart = 3;
	/// <summary>
	/// time after points have been given for when the ball will return to the launch point
	/// </summary>
	public float m_SecondsAfterScore = 2;
	/// <summary>
	/// how long after hitting the ball will it start being used for scoring again
	/// </summary>
	public float m_SecondsOfBallImmunity = 0.1f;
	/// <summary>
	/// time of actions used for second counting
	/// </summary>
	private float m_Time;

	/// <summary>
	/// storage of the launch velocity of the ball 
	/// </summary>
	private Vector3 m_LaunchVel;

	/// <summary>
	/// reference to moveTowards script to start and stop automated movement of ball
	/// </summary>
	private MoveTowards m_MoveTowards;

	/// <summary>
	/// reference to GameManager to tell it when there is a score or when to reset the ball
	/// </summary>
	private GameManager m_Gm;

	/// <summary>
	/// reference to the balls rigidbody to set velocity of launch
	/// </summary>
	private Rigidbody m_Rb;

	/// <summary>
	/// reference to the camera effects script to tell it information about game events
	/// </summary>
	private CameraEffects m_CameraEffects;

	public void Start() {
		m_Rb = GetComponent<Rigidbody>();
		m_Gm  = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		m_MoveTowards = GetComponent<MoveTowards>();
		Reset();

		m_CameraEffects = Camera.main.GetComponent<CameraEffects>();
	}

	/// <summary>
	/// resets everything needed for the ball to be used again
	/// </summary>
	public void Reset() {
		m_Rb.isKinematic = true;
		m_MoveTowards.enabled = true;
		m_Time = 0;
		m_CanHit = false;
		m_BallResetting = false;
		m_BouncesOnCurrentSide = 0;
		m_CurrSide = 0;
	}

	/// <summary>
	/// after the ball has been moved into position
	/// work out where to send it, wait a few seconds then launch the ball
	/// </summary>
	public void Update() {
		if (m_BallImmunity) {
			if (m_Time + m_SecondsOfBallImmunity <= Time.time) {
				m_BallImmunity = false;
			}
		}
		if (m_CanHit) {
			return;
		}
		if (m_MoveTowards.m_Complete) {
			m_MoveTowards.enabled = false;
			if (m_Time == 0) {
				m_Time = Time.time;
				CalculateLaunch();
			}
			else {
				if(m_Time+m_SecondsTillStart <= Time.time) {
					m_Rb.isKinematic = false;
					m_Rb.velocity = m_LaunchVel;
					m_CanHit = true;
					GetComponent<BallLandPerdiction>().m_CanUpdatePosition = true;
				}
			}
		}

		if(m_BallResetting) {
			m_Rb.velocity *= 0.97f;
			if (m_Time+m_SecondsAfterScore <= Time.time) {
				m_Gm.ResetBall(gameObject);				
			}
		}

	}

	/// <summary>
	/// bounce the ball, and also check if the ball has
	/// bounced twice
	/// bounced out of bounds
	/// or swapped sides
	/// </summary>
	/// <param name="collision"></param>
	public void OnCollisionEnter(Collision collision) {
		if (!collision.transform.CompareTag("Ground")) {
			return;
		}
		if (m_BallImmunity) {
			print("Player hit ball but it still landed on their side");
			return;
		}


		if (!m_CanHit) {
			m_CameraEffects.ShakeRun(0.1f, 0.1f);
			return;
		}

		//run hit ground shake
		m_CameraEffects.ShakeRun(0.1f, 0.27f);


		if (m_BouncesOnCurrentSide == 0) {
			Vector3 pos = transform.position;
			bool outOfBounds = (pos.x > m_Map.m_Width) || (pos.x < -m_Map.m_Width) || (pos.z > m_Map.m_Height) || (pos.z < -m_Map.m_Height);
			if (outOfBounds) {
				print("OUT OF BOUNDS!!!");
				ResetBall();
				return;
			}
		}

		int side = GetSide(transform.position);
		if (side == m_CurrSide) {
			m_BouncesOnCurrentSide++;
			if (m_BouncesOnCurrentSide > 1) {
				print("TOO MANY BOUNCES");
				m_CurrSide *= -1;
				ResetBall();
			}
		}
		else {
			if (m_CurrSide != 0) {
				//other side, lose game
				print("YOU HIT THE SAME SIDE!!!");
				ResetBall();
			}
			m_BouncesOnCurrentSide = 1;
			m_CurrSide = side;
		}
	}

	/// <summary>
	/// the ball has been hit, reset currentSide
	/// </summary>
	public void BallHit() {
		m_BouncesOnCurrentSide = 0;
		m_CurrSide = -GetSide(transform.position);

		//ball immunity from scoring
		m_BallImmunity = true;
		m_Time = Time.time;
	}

	/// <summary>
	/// which side is the ball on
	/// </summary>
	/// <returns>returns -1 or 1 for z sides</returns>
	public static int GetSide(Vector3 a_Pos) {
		if (a_Pos.z > 0) {
			return 1;
		}
		return -1;
	}

	/// <summary>
	/// ball is in position, work out where to send it
	/// </summary>
	private void CalculateLaunch() {
		Vector3 pos = Vector3.zero;

		float xPos = Random.Range(-m_Map.m_Width + m_Map.m_Offset, m_Map.m_Width - m_Map.m_Offset);
		float zPos = Random.Range(1, m_Map.m_Height - m_Map.m_Offset);
		if (Random.value > 0.5f) {
			zPos *= -1;
		}

		pos = new Vector3(xPos, 0, zPos);

		m_LaunchVel = BallLandPerdiction.CalcBallisticVelocityVector(transform.position, pos, 70);

		GameObject.FindGameObjectWithTag("HitPos").transform.position = pos;
		
		
	}

	/// <summary>
	/// the game has been lost, reset game
	/// </summary>
	private void ResetBall() {
		if(!m_CanHit || m_BallResetting) {
			return;
		}
		m_BallResetting = true;
		m_CanHit = false;
		m_Time = Time.time;
		m_Gm.AddScore(m_CurrSide);

		GameObject.FindGameObjectWithTag("HitPos").transform.position = new Vector3(0, 0, -100);
		GetComponent<BallLandPerdiction>().m_CanUpdatePosition = false;

		m_CameraEffects.ChromaticRun();

		/*
			GameObject newBall = Instantiate(gameObject);

			Ball ball = newBall.GetComponent<Ball>();
			ball.m_BouncesOnCurrentSide = 0;
			ball.m_LastSide = 0;

			newBall.transform.position = Vector3.zero;

			Destroy(gameObject, 10);
			gameObject.layer = LayerMask.NameToLayer("Default");
			GetComponent<BallLandPerdiction>().enabled = false;
			enabled = false;
		*/
	}
}
