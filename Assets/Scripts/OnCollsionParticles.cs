﻿using UnityEngine;
using System.Collections;

/// <summary>
/// spawns particles on collision with set time for a Destroy
/// </summary>
public class OnCollsionParticles : MonoBehaviour {

	/// <summary>
	/// reference to particles to spawn
	/// </summary>
	public ParticleSystem m_Particles;
	/// <summary>
	/// time until particles get deleted
	/// </summary>
	public float m_ParticleTimer;

	/// <summary>
	/// reference to the last particle object that was spawned
	/// </summary>
	[HideInInspector]
	public GameObject m_LastParticleSystem; 

	/// <summary>
	/// spawn a particle system at transform location and if there is a timer, set it up to delete after that time
	/// </summary>
	/// <param name="collision"></param>
	public void OnCollisionEnter(Collision collision) {
		m_LastParticleSystem = (Instantiate(m_Particles, transform.position,transform.rotation) as ParticleSystem).gameObject;

		if (m_ParticleTimer != 0) {
			Destroy(m_LastParticleSystem, m_ParticleTimer);
		}

		//m_LastParticleSystem = particles;

	}
}
