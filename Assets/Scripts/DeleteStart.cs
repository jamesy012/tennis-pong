﻿using UnityEngine;
using System.Collections;

/// <summary>
/// deletes objects on awake
/// </summary>
public class DeleteStart : MonoBehaviour {

	/// <summary>
	/// list of things to be deleted set within editor
	/// </summary>
	public GameObject[] m_DeleteList;

	/// <summary>
	/// deletes objects in m_DeleteList
	/// and destroys self
	/// </summary>
	void Awake () {
		for(int i = 0; i < m_DeleteList.Length; i++) {
			Destroy(m_DeleteList[i]);
		}
		Destroy(gameObject);
	}

}
