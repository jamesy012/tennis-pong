﻿using UnityEngine;
using System.Collections;

/// <summary>
/// stores data between scenes
/// </summary>
public class DataManager {

	/// <summary>
	/// default controls: in this order
	/// UP,RIGHT,DOWN,LEFT,HIT
	/// </summary>
	public static KeyCode[,] m_PlayerCodes = { { KeyCode.W, KeyCode.D, KeyCode.S, KeyCode.A, KeyCode.LeftShift }, { KeyCode.UpArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightShift } };
}
