﻿using UnityEngine;
using System.Collections;

/// <summary>
/// after the game has been won, this sets a timer and after that time it will return to the main menu
/// </summary>
public class WinGame : MonoBehaviour {
	/// <summary>
	/// seconds to wait till scene change
	/// </summary>
	public float m_SecondsAfterWin = 4.0f;
	/// <summary>
	/// when the game is won, this is set to Time.time
	/// </summary>
	private float m_WinTime;

	/// <summary>
	/// list of objects that should but disabled after winning the game
	/// </summary>
	public GameObject[] m_ScriptsToTurnOffAfterWin;
	/// <summary>
	/// has the game been won?
	/// </summary>
	public static bool m_GameWon = false;
	
	/// <summary>
	/// checks timer to see if it should go back to the main menu
	/// </summary>
	void Update () {
		if(m_WinTime == 0) {
			m_WinTime = Time.time;
		}
		if(m_WinTime+m_SecondsAfterWin < Time.time) {
			m_GameWon = false;
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
	}

	/// <summary>
	/// tells this script to start
	/// disabled object in m_ScriptsToTurnOffAfterWin array
	/// </summary>
	public void GameWon() {
		m_GameWon = true;
		for (int i = 0; i < m_ScriptsToTurnOffAfterWin.Length; i++) {
			m_ScriptsToTurnOffAfterWin[i].SetActive(false);
		}

		//sets this script to be enabled
		enabled = true;
	}
}
