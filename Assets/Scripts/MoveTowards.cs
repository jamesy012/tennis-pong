﻿using UnityEngine;
using System.Collections;

/// <summary>
/// moves object towards another.
/// to reset re enable object or set m_Complete to false
/// </summary>
public class MoveTowards : MonoBehaviour {

	/// <summary>
	/// transform to move this object towards
	/// </summary>
	public Transform m_Target;

	/// <summary>
	/// speed times by delta time
	/// </summary>
	public float m_Speed = 3;

	/// <summary>
	/// does this position equal the starting position
	/// </summary>
	public bool m_Complete = false;

	/// <summary>
	/// move object towards m_Target and check if the object is at target.
	/// wont run of m_Complete is true
	/// </summary>
	void Update() {
		if (m_Complete) {
			return;
		}

		transform.position = Vector3.MoveTowards(transform.position, m_Target.position, m_Speed * Time.deltaTime);

		if (transform.position == m_Target.position) {
			m_Complete = true;
		}
	}

	/// <summary>
	/// reset's the script
	/// </summary>
	public void OnEnable() {
		m_Complete = false;
	}
}
