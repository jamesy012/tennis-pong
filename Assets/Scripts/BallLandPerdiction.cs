﻿using UnityEngine;
using System.Collections;

/// <summary>
/// update the location of a transform with a predicted landing pos of this gameObject
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class BallLandPerdiction : MonoBehaviour {

	/// <summary>
	/// transform to change to tell the user where the ball is landing
	/// </summary>
	public Transform m_LandPositionObject;

	/// <summary>
	/// rigidbody of this gameObject to get velocity
	/// </summary>
	private Rigidbody m_Rigid;

	/// <summary>
	/// a bool it check if this script should update the landing pos transform
	/// </summary>
	[HideInInspector]
	public bool m_CanUpdatePosition = true;

	/// <summary>
	/// gets rigidbody
	/// </summary>
	void Start() {
		m_Rigid = GetComponent<Rigidbody>();
	}

	/// <summary>
	/// works out next location the object will take at velocity after bounce
	/// and sets m_LandPositionObject's transform at that position
	/// wont run of m_CanUpdatePosition is false
	/// </summary>
	/// <param name="collision">unused</param>
	public void OnCollisionExit(Collision collision) {
		if (m_Rigid == null) {
			return;
		}
		if (!m_CanUpdatePosition) {
			return;
		}

		Vector3 pos = transform.position;

		//float v = m_Rigid.velocity.y;
		//float a = Physics.gravity.y;
		//float d = transform.position.y * -1;
		//
		//float t = -(Mathf.Sqrt(2 * a * d * Mathf.Pow(v, 2)) + v) / a;
		//pos += m_Rigid.velocity * t;

		float v = m_Rigid.velocity.y;
		float a = Physics.gravity.y;


		float t = (v - 0) / (a);
		t *= -2f;

		pos += m_Rigid.velocity * t;
		pos.y = 0;

		m_LandPositionObject.transform.position = pos;

	}

	/// <summary>
	/// calculates the velocity a object will need to travel from source to target and a certain angle
	/// </summary>
	/// <param name="source">starting position of object</param>
	/// <param name="target">ending pos of object</param>
	/// <param name="angle">what angle should we launch at</param>
	/// <returns>velocity of object to reach target</returns>
	static public Vector3 CalcBallisticVelocityVector(Vector3 source, Vector3 target, float angle) {
		Vector3 direction = target - source;            // get target direction
		float h = direction.y;                                            // get height difference
		direction.y = 0;                                                // remove height
		float distance = direction.magnitude;                            // get horizontal distance
		float a = angle * Mathf.Deg2Rad;                                // Convert angle to radians
		direction.y = distance * Mathf.Tan(a);                            // Set direction to elevation angle
		distance += h / Mathf.Tan(a);                                        // Correction for small height differences

		// calculate velocity
		float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
		return velocity * direction.normalized;
	}
}
