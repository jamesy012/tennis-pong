﻿using UnityEngine;
using System.Collections;

/// <summary>
/// returns game back to the main menu
/// </summary>
public class BackToMenu : MonoBehaviour {
	
	/// <summary>
	/// checks if escape was pressed and if it was returns to the menu
	/// </summary>
	void Update () {
		if (Screen.fullScreen) {
			return;
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
	}
}
