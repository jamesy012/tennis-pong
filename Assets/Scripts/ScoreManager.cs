﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/// <summary>
/// does all the scoring and text handing for score
/// </summary>
public class ScoreManager : MonoBehaviour {
	/// <summary>
	/// values of scoring
	///	<para> 4 is advantage. 5 is winning </para>
	/// <para>	0, 1,  2,  3,   4,  5 </para>
	///			0, 15, 30, 40, -1, 999
	/// </summary>
	private int[] m_Scores = { 0, 15, 30, 40, -1, 999 };
	/// <summary>
	/// score that references the score of m_Scores
	/// </summary>
	public int[] m_PlayerScore = { 0, 0 };
	/// <summary>
	/// text reference for score for each player
	/// </summary>
	private Text[] m_PlayerText = new Text[2];
	/// <summary>
	/// which side has won the game?
	/// -1 is no one
	/// </summary>
	private int m_SideWon = -1;

	/// <summary>
	/// gets text components and sets the scores text to be starting values
	/// </summary>
	public void Start() {
		m_PlayerText[0] = transform.GetChild(0).GetComponent<Text>();
		m_PlayerText[1] = transform.GetChild(1).GetComponent<Text>();
		SetScore(0);
		SetScore(1);
	}

	/// <summary>
	/// adds score to side and checks if there is a winner
	/// </summary>
	/// <param name="a_Side">which side to add score too</param>
	public void AddScore(int a_Side) {
		m_PlayerScore[a_Side]++;
		if (m_PlayerScore[0] == 4 && m_PlayerScore[1] == 4) {
			m_PlayerScore[1] = 3;
			m_PlayerScore[0] = 3;

			SetScore(0);
			SetScore(1);
		} else {
			SetScore(a_Side);
		}

		CheckWin();
	}

	/// <summary>
	/// checks if either side has won the game
	/// </summary>
	private void CheckWin() {
		if ((m_PlayerScore[0] == 4 && m_PlayerScore[1] != 3) || m_PlayerScore[0] == 5) {
			print("P1 win");
			m_SideWon = 0;
			Reset();
		}
		if ((m_PlayerScore[1] == 4 && m_PlayerScore[0] != 3) || m_PlayerScore[1] == 5) {
			print("P2 win");
			m_SideWon = 1;
			Reset();
		}
	}

	/// <summary>
	/// tells the game controller that there has been a winner
	/// </summary>
	private void Reset() {
		//m_PlayerScore[0] = 0;
		//m_PlayerScore[1] = 0;
		SetScore(0);
		SetScore(1);
		GameObject.FindGameObjectWithTag("GameController").GetComponent<WinGame>().GameWon();
	}

	/// <summary>
	/// sets text for a side, with AD. and WIN if they are at that stage
	/// </summary>
	/// <param name="a_Side">side to change score for</param>
	private void SetScore(int a_Side) {
		if (m_SideWon == a_Side) {
			m_PlayerText[a_Side].text = "WIN";
			return;
		}
		int score = m_Scores[m_PlayerScore[a_Side]];
		if (score == -1) {
			m_PlayerText[a_Side].text = "AD.";
		} else {
			m_PlayerText[a_Side].text = score.ToString();
		}
	}

}
