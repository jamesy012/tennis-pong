﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

/// <summary>
/// changes key information in DataManager using keyboard inputs
/// and sets main menu keys to their new keys
/// </summary>
public class ChangeKey : MonoBehaviour {

	/// <summary>
	/// does the user want to change a key?
	/// </summary>
	private bool m_IsChanging = false;
	/// <summary>
	/// which player is this changing for
	/// </summary>
	private int m_Player;
	/// <summary>
	/// what action is this change for
	/// </summary>
	private int m_Action;

	/// <summary>
	/// rectTransform that holds the keys of each player
	/// should only be a length of 2, one for each player
	/// </summary>
	public RectTransform[] m_PlayerKeyHolders;

	/// <summary>
	/// transform to show and hide depending if the user is setting a lever
	/// </summary>
	public GameObject m_ChangingKeyText;

	/// <summary>
	/// sets up current keys text with ones from DataManager
	/// </summary>
	public void Start() {
		m_ChangingKeyText.SetActive(false);
		//update text to what is stored in the dataManager
		for (int p = 0; p < 2; p++) {
			for (int i = 0; i < 5; i++) {
				UpdateText(p, i);
			}
		}
	}

	/// <summary>
	/// keys if the user has press a key
	/// if they did then it will set the text of the selected letter to be the key that was pressed
	/// </summary>
	void Update() {
		if (!m_IsChanging) {
			return;
		}

		if (Input.anyKeyDown) {

			int keys = System.Enum.GetNames(typeof(KeyCode)).Length;
			for (int i = 0; i < keys; i++) {
				if (Input.GetKeyDown((KeyCode)i)) {
					string keyName = GetText((KeyCode)i);
					if (keyName == "") {
						if ((KeyCode)i != KeyCode.Escape) {
							Debug.Log("Failed to find key: " + (KeyCode)i + ", ID: " + i);
						}
						ResetChange();
						break;
					}
					DataManager.m_PlayerCodes[m_Player, m_Action] = (KeyCode)i;
					Text text = GetKeyText(m_Player,m_Action);
					text.text = keyName;
					m_IsChanging = false;
					m_ChangingKeyText.SetActive(false);
					break;
				}
			}
		}
	}

	/// <summary>
	/// reverts text of selected button to what was in DataManager
	/// sets m_ChangingKeyText and m_IsChanging to false;
	/// </summary>
	private void ResetChange() {
		UpdateText(m_Player, m_Action);
		m_ChangingKeyText.SetActive(false);
		m_IsChanging = false;
	}

	/// <summary>
	/// updates a_Player's player and a_Action's action text to be what is in DataManager if a_Text is null
	/// </summary>
	/// <param name="a_Player">player index</param>
	/// <param name="a_Action">action index</param>
	/// <param name="a_Text">letter to set action text too</param>
	private void UpdateText(int a_Player, int a_Action,string a_Text = null) {
		Text text = GetKeyText(a_Player,a_Action);
		string letterText;
		if (a_Text == null) {
			letterText = GetText(DataManager.m_PlayerCodes[a_Player, a_Action]);
			//if the key was not in the switch then take it from the keyCodes ToString
			if (letterText == "") {
				letterText = DataManager.m_PlayerCodes[a_Player, a_Action].ToString();
			}
		}
		else {
			letterText = a_Text;
		}
		text.text = letterText;
	}

	/// <summary>
	/// gets the text component of the player and action
	/// </summary>
	/// <param name="a_Player">player index</param>
	/// <param name="a_Action">action index</param>
	/// <returns>Text component for action</returns>
	private Text GetKeyText(int a_Player, int a_Action) {
		if (a_Action <= 3) {
			return m_PlayerKeyHolders[a_Player].GetChild(0).GetChild(a_Action).GetComponentInChildren<Text>();
		}
		return m_PlayerKeyHolders[a_Player].GetChild(1).GetChild(0).GetComponentInChildren<Text>();
	}

	/// <summary>
	/// gets a string version of KeyCode
	/// a smaller version then ToString
	/// </summary>
	/// <param name="a_Letter">Letter to get name from</param>
	/// <returns>KeyCode name</returns>
	private string GetText(KeyCode a_Letter) {
		switch (a_Letter) {
			//keys we cant use
			case KeyCode.Escape:
				return "";
			case KeyCode.LeftShift:
				return "Left Shift";
			case KeyCode.LeftControl:
				return "Left Control";
			case KeyCode.LeftAlt:
				return "Left Alt";
			case KeyCode.Space:
				return "Space";
			case KeyCode.RightShift:
				return "Right Shift";
			case KeyCode.RightControl:
				return "Right Control";
			case KeyCode.RightAlt:
				return "Right Alt";
			case KeyCode.Return:
				return "Enter";
			case KeyCode.LeftArrow:
				return "Left Arrow";
			case KeyCode.RightArrow:
				return "Right Arrow";
			case KeyCode.UpArrow:
				return "Up Arrow";
			case KeyCode.DownArrow:
				return "Down Arrow";
			case KeyCode.Keypad0:
			case KeyCode.Keypad1:
			case KeyCode.Keypad2:
			case KeyCode.Keypad3:
			case KeyCode.Keypad4:
			case KeyCode.Keypad5:
			case KeyCode.Keypad6:
			case KeyCode.Keypad7:
			case KeyCode.Keypad8:
			case KeyCode.Keypad9:
				return "KP " + ((int)a_Letter - 256).ToString();
			case KeyCode.Alpha0:
			case KeyCode.Alpha1:
			case KeyCode.Alpha2:
			case KeyCode.Alpha3:
			case KeyCode.Alpha4:
			case KeyCode.Alpha5:
			case KeyCode.Alpha6:
			case KeyCode.Alpha7:
			case KeyCode.Alpha8:
			case KeyCode.Alpha9:
				return ((int)a_Letter - 48).ToString();
			case KeyCode.KeypadDivide:
				return "KP /";
			case KeyCode.KeypadMinus:
				return "KP -";
			case KeyCode.KeypadMultiply:
				return "KP *";
			case KeyCode.KeypadPeriod:
				return "KP .";
			case KeyCode.KeypadPlus:
				return "KP +";
			case KeyCode.KeypadEnter:
				return "KP Enter";
			case KeyCode.CapsLock:
				return "Caps Lock";
			case KeyCode.ScrollLock:
				return "Scroll Lock";
			case KeyCode.Numlock:
				return "Num Lock";
			case KeyCode.F1:
				return "F1";
			case KeyCode.F2:
				return "F2";
			case KeyCode.F3:
				return "F3";
			case KeyCode.F4:
				return "F4";
			case KeyCode.F5:
				return "F5";
			case KeyCode.F6:
				return "F6";
			case KeyCode.F7:
				return "F7";
			case KeyCode.F8:
				return "F9";
			case KeyCode.F10:
				return "F10";
			case KeyCode.F11:
				return "F11";
			case KeyCode.F12:
				return "F12";
			case KeyCode.Pause:
				return "Pause/Break";
			case KeyCode.AltGr:
				return "Alt Gr";
			case KeyCode.Menu:
				return "Menu";
			case KeyCode.Tab:
				return "Tab";
			case KeyCode.Backspace:
				return "BackSpace";
			case KeyCode.Insert:
				return "Insert";
			case KeyCode.Home:
				return "Home";
			case KeyCode.PageUp:
				return "Page Up";
			case KeyCode.PageDown:
				return "Page Down";
			case KeyCode.End:
				return "End";
			case KeyCode.Delete:
				return "Delete";
			case KeyCode.BackQuote:
				return "`";
			case KeyCode.Minus:
				return "-";
			case KeyCode.Equals:
				return "=";
			case KeyCode.LeftBracket:
				return "[";
			case KeyCode.RightBracket:
				return "]";
			case KeyCode.Backslash:
				return "\\";
			case KeyCode.Semicolon:
				return ";";
			case KeyCode.Quote:
				return "'";
			case KeyCode.Comma:
				return ",";
			case KeyCode.Period:
				return ".";
			case KeyCode.Slash:
				return "/";

		}
		return Input.inputString.ToUpper();
	}

	/// <summary>
	/// takes a number between 0-9 and sets m_Player and m_Action to their corresponding number.
	/// sets script up for changing the key
	/// </summary>
	/// <param name="a_Index">action index, p1 is 0-4. p2 is 5-9</param>
	public void KeyToChange(int a_Index) {
		if (m_IsChanging) {
			ResetChange();
		}
		if (a_Index >= 5) {
			m_Player = 1;
		}
		else {
			m_Player = 0;
		}

		int actionNum =  a_Index - (5 * m_Player);

		if(actionNum >= 5 || actionNum < 0) {
			return;
		}


		m_Action = actionNum;
		m_IsChanging = true;
		updateChangingLetterText();
		UpdateText(m_Player, m_Action, "_");
	}

	/// <summary>
	/// sets text of m_ChangingKeyText to be what action and player is getting edited
	/// </summary>
	private void updateChangingLetterText() {
		m_ChangingKeyText.SetActive(true);
		string action = "Unknown";
		switch (m_Action) {
			case 0:
			case 1:
			case 2:
			case 3:
				action = "Movement";
				break;
			case 4:
				action = "Power/Hit";
				break;
		}
		m_ChangingKeyText.GetComponent<Text>().text = "Changing key for player " + (m_Player + 1) + " for the action of " + action;
	}
}
