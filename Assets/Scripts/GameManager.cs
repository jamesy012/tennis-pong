﻿using UnityEngine;
using System.Collections;

/// <summary>
/// manages:
/// score, resetting ball, letting the game win
/// </summary>
public class GameManager : MonoBehaviour {
	/*
	/// <summary>
	/// reference to the fake ball prefab
	/// </summary>
	public GameObject m_FakeBall;
	/// <summary>
	/// reference to the normal ball prefab
	/// </summary>
	public GameObject m_Ball;
	*/

	/// <summary>
	/// reference to the point where the ball gets launched
	/// </summary>
	public Transform m_LaunchPoint;

	/// <summary>
	/// reference to the score manager to tell it that score should be added
	/// </summary>
	public ScoreManager m_Sm;

	/// <summary>
	/// sets the winGame component to be disabled
	/// </summary>
	public void Start() {
		GetComponent<WinGame>().enabled = false;
	}

	/// <summary>
	/// tells score manager to add score to a side
	/// </summary>
	/// <param name="a_Side"></param>
	public void AddScore(int a_Side) {
		m_Sm.AddScore(a_Side != 1 ? 0 : 1);
	}

	/// <summary>
	/// resets scripts inside of a_Ball to allow game play to start again
	/// </summary>
	/// <param name="a_Ball"></param>
	public void ResetBall(GameObject a_Ball) {
		//**** create fake ball
		/*
		GameObject fakeBall = Instantiate(m_FakeBall, a_Ball.transform.position, a_Ball.transform.rotation) as GameObject;
		Rigidbody fakeRigid = fakeBall.GetComponent<Rigidbody>();
		fakeRigid.isKinematic = false;
		fakeRigid.velocity = a_Ball.GetComponent<Rigidbody>().velocity;
		//add random
		fakeRigid.velocity += new Vector3(Random.Range(-1, 1), Random.Range(0, 5), Random.Range(-1, 1)) * Random.Range(1, 5);
		fakeRigid.rotation *= Quaternion.Euler(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)) * Random.Range(1, 360));

		//Destroy(a_Ball);
		Destroy(fakeBall, 10);
		*/

		//**** reset current ball

		a_Ball.GetComponent<MoveTowards>().m_Target = m_LaunchPoint;

		//if the game has been won, disable scripts to stop the ball from being used
		if (WinGame.m_GameWon) {
			a_Ball.GetComponent<Ball>().enabled = false;
			a_Ball.GetComponent<BallLandPerdiction>().enabled = false;
			a_Ball.GetComponent<Rigidbody>().useGravity = false;
			a_Ball.GetComponent<MoveTowards>().enabled = true;
		}
		else {
			a_Ball.GetComponent<Ball>().Reset();
		}

		//Camera.main.GetComponent<CameraView>().m_Target = a_Ball.transform;	

	}
}
