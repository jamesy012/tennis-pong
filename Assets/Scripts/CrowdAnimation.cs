﻿using UnityEngine;
using System.Collections;

/// <summary>
/// gets a random number for a animation out of m_Animations
/// </summary>
public class CrowdAnimation {

	/// <summary>
	/// max animations
	/// </summary>
	public static int m_Animations = 2;

	/// <summary>
	/// gets a random number between 0 and m_Animations
	/// always has a 50% chance to pick the first animation
	/// </summary>
	/// <returns>number between 0 and m_Animations</returns>
	public static int GetRandomAnimation() {
		if(Random.value >= 0.5) {
			return 0;
		}
		return Random.Range(0, m_Animations);
	}
}
