﻿using UnityEngine;
using System.Collections;


/// <summary>
/// moves the player
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class MovePlayer : MonoBehaviour {

	/// <summary>
	/// reference to Rigidbody to move object
	/// </summary>
	private Rigidbody m_Rigid;

	/// <summary>
	/// speed at which to move
	/// (by DeltaTime)
	/// </summary>
	public float m_Speed = 1500;
	/// <summary>
	/// speed at which to move when holding down hit key
	/// (by DeltaTime)
	/// </summary>
	public float m_SpeedWhenPowering = 700;

	/// <summary>
	/// max speed of object
	/// (not times by deltaTime)
	/// </summary>
	public float m_MaxSpeed =5;

	/// <summary>
	/// reference to HitBall to check if hit key is down
	/// </summary>
	private HitBall m_HitBall;

	/// <summary>
	/// what player is this?
	/// </summary>
	public int m_PlayerNum = 0;

	/// <summary>
	/// gets rigidbody component and hitBall component
	/// </summary>
	void Start () {
		m_Rigid = GetComponent<Rigidbody>();
		m_HitBall = GetComponentInChildren<HitBall>();
	}
	
	/// <summary>
	/// gets movement and add force to the rigidbody in direction of movement
	/// </summary>
	void Update () {
		//Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		Vector3 movement = GetMovement(m_PlayerNum);

		float speed = m_Speed;

		if (m_HitBall.m_IsHitting) {
			speed = m_SpeedWhenPowering;
		}

		movement *= speed * Time.deltaTime;
		m_Rigid.AddForce(movement,ForceMode.Acceleration);

		if (m_Rigid.velocity.magnitude >= m_MaxSpeed) {
			print("MAX SPEEd");
			m_Rigid.velocity = m_Rigid.velocity.normalized * m_MaxSpeed;
		}
	}

	/// <summary>
	/// gets players movement using a_Player from the DataManager in a unitVector 
	/// </summary>
	/// <param name="a_Player">which player to get key information from</param>
	/// <returns>unit vector of players key information</returns>
	public static Vector3 GetMovement(int a_Player) {
		Vector3 amount = Vector3.zero;
		if (a_Player >=2 || a_Player <=-1) {
			return amount;
		}


		if (Input.GetKey(DataManager.m_PlayerCodes[a_Player, 0])) {
			amount.z += 1;
		}
		if (Input.GetKey(DataManager.m_PlayerCodes[a_Player, 1])) {
			amount.x += 1;
		}
		if (Input.GetKey(DataManager.m_PlayerCodes[a_Player, 2])) {
			amount.z -= 1;
		}
		if (Input.GetKey(DataManager.m_PlayerCodes[a_Player, 3])) {
			amount.x -= 1;
		}

		return amount.normalized;
	}


}
