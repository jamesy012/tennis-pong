﻿using UnityEngine;
using System.Collections;

/// <summary>
/// spawns objects in a line
/// </summary>
public class CrowdSpawner : MonoBehaviour {

	/// <summary>
	/// float max distance from object to spawn till
	/// </summary>
	public float m_SpawnTill;

	/// <summary>
	/// distance between each object
	/// </summary>
	public float m_Increment;

	/// <summary>
	/// object to spawn
	/// </summary>
	public GameObject m_CrowdObject;

	/// <summary>
	/// spawn objects in a line from position and rotation
	/// </summary>
	void Start() {
		for (float i = 0; i <= 1; i += m_Increment / m_SpawnTill) {
			Instantiate(m_CrowdObject, transform.position + (m_SpawnTill * transform.forward) * i, transform.rotation*m_CrowdObject.transform.rotation, transform);
		}
		Instantiate(m_CrowdObject, transform.position + (m_SpawnTill * transform.forward), transform.rotation * m_CrowdObject.transform.rotation, transform);
	}

	/// <summary>
	/// demonstrates how object will spawn
	/// </summary>
	public void OnDrawGizmosSelected() {
		if (m_Increment <= 0) {
			return;
		}
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.position + m_SpawnTill * transform.forward, 1.0f);
		for(float i = 0; i< 1; i += m_Increment/m_SpawnTill) {
			Gizmos.DrawSphere(transform.position + (m_SpawnTill * transform.forward)*i, 0.2f);
		}
	}
}
