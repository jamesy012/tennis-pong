﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

/// <summary>
/// does camera related effects such as screen shake and chromatic aberration
/// </summary>
[RequireComponent(typeof(VignetteAndChromaticAberration))]
public class CameraEffects : MonoBehaviour {

	/// <summary>
	/// default value of the chromatic script
	/// </summary>
	private float m_ChromaticDefault;
	/// <summary>
	/// curve for the chromatic aberration
	/// </summary>
	public AnimationCurve m_ChromaticCurve;
	/// <summary>
	/// when getting the value from the curve, it is multiplied by this number
	/// </summary>
	public float m_ChromaticMultiplyer = 1.0f;
	/// <summary>
	/// how quick do we go through the curve
	/// </summary>
	public float m_ChromaticSpeed;
	/// <summary>
	/// how far are we through the curve
	/// </summary>
	private float m_ChromaticTime;
	/// <summary>
	/// reference to chromatic aberration
	/// </summary>
	private VignetteAndChromaticAberration m_ChromaticAberation;
	/// <summary>
	/// are we doing a chromatic aberration atm?
	/// </summary>
	private bool m_ChromaticRunning = false;

	/// <summary>
	/// a timer that counts down to 0
	/// </summary>
	public float m_ShakeTimeLeft;
	public float m_ShakePower = 1.0f;


	// Use this for initialization
	void Start () {
		m_ChromaticAberation = GetComponent<VignetteAndChromaticAberration>();
		m_ChromaticDefault = m_ChromaticAberation.chromaticAberration;
	}
	
	// Update is called once per frame
	void Update () {
		ChromaticUpdate();
		ShakeUpdate();
	}

	private void ChromaticUpdate() {
		if (m_ChromaticRunning) {
			m_ChromaticTime += m_ChromaticSpeed * Time.deltaTime;
			if (m_ChromaticTime > 1) {
				m_ChromaticTime = 1;
				m_ChromaticRunning = false;
			}
			float val = m_ChromaticCurve.Evaluate(m_ChromaticTime) * m_ChromaticMultiplyer;
			m_ChromaticAberation.chromaticAberration = m_ChromaticDefault + val;

		}
	}

	public void ChromaticRun() {
		m_ChromaticRunning = true;
		m_ChromaticTime = 0;
	}

	private void ShakeUpdate() {
		if (m_ShakeTimeLeft > 0) {
			Vector3 shake = Random.insideUnitSphere * m_ShakePower;
			shake.z = 0;
			transform.localPosition += shake;

			m_ShakeTimeLeft -= Time.deltaTime;
		}
	}

	public void ShakeRun(float a_Time,float a_Power = .2f) {
		m_ShakeTimeLeft = a_Time;
		m_ShakePower = a_Power;
	}
}
