﻿using UnityEngine;
using System.Collections;

/// <summary>
/// contains size of the map
/// </summary>
public class Map : MonoBehaviour {

	/// <summary>
	/// width of map in unity units
	/// </summary>
	public float m_Width = 5;
	/// <summary>
	/// height of map in unity units
	/// </summary>
	public float m_Height = 5;

	/// <summary>
	/// offset in width and height in unity units.
	/// used to add that little extra but so hits can miss
	/// </summary>
	public float m_Offset = 0.5f;
}
